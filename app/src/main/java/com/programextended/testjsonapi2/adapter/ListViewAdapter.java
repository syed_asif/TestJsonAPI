package com.programextended.testjsonapi2.adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.programextended.jsonapi.utility.ImageLoader;
import com.programextended.testjsonapi2.MainActivity;
import com.programextended.testjsonapi2.view.SingleItemView;
import com.programextended.testjsonapi2.R;

public class ListViewAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<LinkedHashMap<String, String>> data;
    ImageLoader imageLoader;
    LinkedHashMap<String, String> resultp = new LinkedHashMap<String, String>();

    public ListViewAdapter(Context context, ArrayList<LinkedHashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView rank;
        TextView country;
        TextView population;
        ImageView flag;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.listview_item, parent, false);
        resultp = data.get(position);

        rank = (TextView) itemView.findViewById(R.id.rank);
        country = (TextView) itemView.findViewById(R.id.country);
        population = (TextView) itemView.findViewById(R.id.population);

        flag = (ImageView) itemView.findViewById(R.id.flag);

        rank.setText(resultp.get(MainActivity.RANK));
        country.setText(resultp.get(MainActivity.COUNTRY));
        population.setText(resultp.get(MainActivity.POPULATION));

        imageLoader.DisplayImage(resultp.get(MainActivity.FLAG), flag);

        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                resultp = data.get(position);
                Intent intent = new Intent(context, SingleItemView.class);
                intent.putExtra("rank", resultp.get(MainActivity.RANK));
                intent.putExtra("country", resultp.get(MainActivity.COUNTRY));
                intent.putExtra("population",resultp.get(MainActivity.POPULATION));
                intent.putExtra("flag", resultp.get(MainActivity.FLAG));

                context.startActivity(intent);
            }
        });
        return itemView;
    }
}