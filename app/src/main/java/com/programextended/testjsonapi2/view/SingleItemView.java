package com.programextended.testjsonapi2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.programextended.jsonapi.utility.ImageLoader;
import com.programextended.testjsonapi2.R;

public class SingleItemView extends Activity {
    String rank;
    String country;
    String population;
    String flag;
//    String position;
    ImageLoader imageLoader = new ImageLoader(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.singleitemview);

        Intent i = getIntent();
        rank = i.getStringExtra("rank");
        country = i.getStringExtra("country");
        population = i.getStringExtra("population");
        flag = i.getStringExtra("flag");

        TextView txtrank = (TextView) findViewById(R.id.rank);
        TextView txtcountry = (TextView) findViewById(R.id.country);
        TextView txtpopulation = (TextView) findViewById(R.id.population);

        ImageView imgflag = (ImageView) findViewById(R.id.flag);

        txtrank.setText(rank);
        txtcountry.setText(country);
        txtpopulation.setText(population);

        imageLoader.DisplayImage(flag, imgflag);
    }
}