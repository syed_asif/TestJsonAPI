# TestJsonAPI

This project is used to test the [json api for android](https://gitlab.com/syed_asif/AndroidJsonAPI)

## Built With

* **Android Studio - 3.1.x**

## License

Copyright 2018 Syed Asif Mahmood

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Acknowledgments

The tutorial that was followed to develop the api.
https://www.androidbegin.com/tutorial/android-json-parse-images-and-texts-tutorial/
